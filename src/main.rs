use std::process;
use std::error::Error;
use std::env;
use std::io::Read;
use std::collections::HashMap;

const NB_CELLS: usize = 30_000;

fn die(code: i32, msg: &str) -> ! {
    eprintln!("{}", msg);
    process::exit(code);
}

/// Builds a hashmap which specifies for each '[' and ']' where to jump bac
fn build_jump_table(input: &str) -> HashMap<usize, usize> {
    let mut ret = HashMap::new();

    for i in 0..input.len() {
        match input.chars().nth(i).unwrap() {
            '[' => {
                let mut nb_brackets = 1;
                let mut matching_ind = None;
                for j in i+1..input.len() {
                    match input.chars().nth(j).unwrap() {
                        '[' => nb_brackets += 1,
                        ']' => nb_brackets -= 1,
                        _ => continue
                    }

                    if nb_brackets == 0 {
                        matching_ind = Some(j);
                        break;
                    }
                }
                if let Some(ind) = matching_ind {
                    // '[' is at i, ']' is at ind
                    ret.insert(i, ind + 1);
                    ret.insert(ind, i + 1);
                } else {
                    die(1, "Error: malformed input: each '[' must have a matching ']'");
                }
            },
            ']' => {
                //check if it has a matching entry in the table
                if let None = ret.get(&i) {
                    die(1, "Error: malformed input: each ']' must have a matching '['");
                }
            },
            _ => ()
        }
    }

    ret
}

fn inc_cell_ptr(cells: &[u8], cur: &mut usize) {
    if *cur >= cells.len() - 1 {
        if cfg!(feature = "cell-ptr-no-wrap") {
            die(1, "Error: cell pointer overflow");
        } else {
            *cur = 0
        }
    } else {
        *cur += 1;
    }
}

fn dec_cell_ptr(cells: &[u8], cur: &mut usize) {
    if *cur == 0 {
        if cfg!(feature = "cell-ptr-no-wrap") {
            die(1, "Error: cell pointer underflow");
        } else {
            *cur = cells.len() - 1
        }
    } else {
        *cur -= 1;
    }
}

fn inc_cell_val(cells: &mut [u8], cur: usize) {
    if cfg!(feature = "cell-val-no-wrap") {
        cells[cur] = if let Some(nb) = cells[cur].checked_add(1) {
            nb
        } else {
            die(1, "Error: cell value overflow");
        }
    } else {
        cells[cur] = cells[cur].wrapping_add(1);
    }
}

fn dec_cell_val(cells: &mut [u8], cur: usize) {
    if cfg!(feature = "cell-val-no-wrap") {
        cells[cur] = if let Some(nb) = cells[cur].checked_sub(1) {
            nb
        } else {
            die(1, "Error: cell value underflow");
        }
    } else {
        cells[cur] = cells[cur].wrapping_sub(1);
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        die(1, "Usage: <bin> <input_file>");
    }

    let input = std::fs::read_to_string(&args[1])?;
    let input: String = input.chars()
        .filter(|x| match *x {
            '>'|'<'|'+'|'-'|'.'|','|'['|']' => true,
            _ => false
        }).collect();

    //position in input
    let mut cur_char: usize = 0;
    //position in cells
    let mut cur: usize = 0;

    let jump_table = build_jump_table(&input[..]);
    let mut cells: [u8; NB_CELLS] = [0; NB_CELLS];

    while let Some(char) = input.chars().nth(cur_char) {
        match char {
            '>' => { inc_cell_ptr(&cells, &mut cur) },
            '<' => { dec_cell_ptr(&cells, &mut cur) },
            '+' => { inc_cell_val(&mut cells, cur) },
            '-' => { dec_cell_val(&mut cells, cur) },
            '.' => { print!("{}", cells[cur] as char); },
            ',' => {
                if let Some(c) = std::io::stdin().bytes().next() {
                    let c = c.unwrap();
                    cells[cur] = c;
                }
            },
            '[' => {
                if cells[cur] == 0 {
                    //jump past the matching ']'
                    cur_char = jump_table[&cur_char];
                    continue;
                }
            },
            ']' => {
                if cells[cur] != 0 {
                    //jump back to the matching '['
                    cur_char = jump_table[&cur_char];
                    continue;
                }
            },
            //It's a comment, ignore
            _ => ()
        };

        cur_char += 1;
    }

    Ok(())
}
