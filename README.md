# bf_interpreter

This program interprets brainfuck programs.
   
## Build
   
To build it, you can `cargo build --release`.

There are optional feature flags that you can enable during compilation:
- Disable call pointer wrapping with `--features cell-ptr-no-wrap`
- Disable call value wrapping with `--features cell-val-no-wrap`
   
## Usage

```bash
   ./bin </path/to/brainfuck/program>
```
